package LogsToDB;


import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
public class LogController {
    @Autowired
    private LogRepository logRepository;

    //return all logs
    @GetMapping("/logs")
    public ResponseEntity<Iterable<Log>> getAll() {
        Iterable<Log> logs = logRepository.findAll();
        if (logs.spliterator().getExactSizeIfKnown() == 0){
            return new ResponseEntity<>(logs,HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Iterable<Log>>(logs,HttpStatus.OK);
    }

    //saves log with given content to database
    @GetMapping("/logs/{content}")
    public @ResponseBody Log getBySender(@PathVariable String content){
        Log log = new Log();
        log.setContent(content);
        return logRepository.save(log);
    }

    //return logs by sender or id
   /* @GetMapping("/logs/{sender}")
    public ResponseEntity<String> getBySender(@PathVariable String sender) throws NotFoundException {
        JsonMapper mapper = new JsonMapper();

        if (isNumeric(sender)) {  //checking if id in param
            int id = Integer.parseInt(sender);
            Optional<Log> opt = logRepository.findById(id);
            if(!opt.isPresent()) //no log with this id
                return new ResponseEntity<String>("404",HttpStatus.NOT_FOUND);

            //log with given id exists
            Log log = opt.get();
            return new ResponseEntity<String>(mapper.LogToJson(log), HttpStatus.OK);
        }

        //param is not id, returning logs by sender if exists
        List<Log> logs = logRepository.findBySender(sender);
        if (logs.isEmpty())
            return new ResponseEntity<String>(mapper.LogListToJson(logs),HttpStatus.NOT_FOUND);
        return new ResponseEntity<String>(mapper.LogListToJson(logs), HttpStatus.OK);
    } */


    //post new log with given param in path
/*    @PostMapping("/logs")
    public @ResponseBody Log add(@RequestParam String sender,@RequestParam String receiver,@RequestParam String content){
        Log log = new Log();
        log.setSender(sender);
        log.setReceiver(receiver);
        log.setContent(content);
        return logRepository.save(log);
    }  */

    //add logs from Json List given in POST
    @PostMapping("/logs")
    public @ResponseBody Iterable<Log> addJSON(@RequestBody String content) {

        JsonMapper mapper = new JsonMapper(); //mapper for parsing to Logs

        try {
            List<Log> logs = mapper.JsonToLoglist(content);
            logRepository.saveAll(logs);
            return logs;
        } catch (IOException e) {
            throw new IllegalArgumentException("Json string is invalid, make sure its a list and all fields spelled correctly");
        }
    }

    //returns true if a string represent an integer
    private boolean isNumeric(String str) {

        try {
            int i =Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
