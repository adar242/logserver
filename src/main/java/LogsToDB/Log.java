package LogsToDB;

import javax.persistence.*;

/**
 * a Log represents a single item in the database, with columns:id,content
 */

@Entity
public class Log {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    @Column(nullable = false)
    private String content;
   /* @Column(nullable = false)
    private String sender;
    @Column(nullable = false)
    private String receiver; */

    public Log(){}


    public int getId() {
        return id;
    }


    public String getContent() {
        return content;
    }

 /*   public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    } */

    public void setContent(String content) {
        this.content = content;
    }
}
