package LogsToDB;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JsonMapper {

    //parse json representation of logs objects to log objects
    public List<Log> JsonToLoglist(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<Log> logs = objectMapper.readValue(json, new TypeReference<List<Log>>(){});
        return logs;
    }

    //parse json representation of log object to log object
    public Log JsonToLog(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Log log = objectMapper.readValue(json, Log.class);
        return log;
    }

    //parse Log to Json string
    public String LogToJson(Log log){
        ObjectMapper objectMapper = new ObjectMapper();
        String output = "couldnt return";
        try {
            output = objectMapper.writeValueAsString(log);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return output;
    }

    public String LogListToJson(Iterable<Log> logs){
        ObjectMapper objectMapper = new ObjectMapper();
        String output = "couldnt return";
        try{
            output = objectMapper.writeValueAsString(logs);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return output;
    }
}
